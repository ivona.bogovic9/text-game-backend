package com.its.backend.repositories;

import com.its.backend.entities.Question;

import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {
    //Question findByTitle(String title);
}
