package com.its.backend.repositories;

import com.its.backend.entities.Character;
import org.springframework.data.repository.CrudRepository;

public interface CharacterRepository extends CrudRepository<Character, Long> {
    Character findByName(String name);
}
