package com.its.backend.controllers;

import java.util.List;

import com.its.backend.entities.Question;
import com.its.backend.repositories.QuestionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class QuestionController {
    @Autowired
    QuestionRepository repo;
    
    @GetMapping("/question")
    //get a list of all users
    public List<Question> list(){
        return (List<Question>) repo.findAll();
    }

    @GetMapping("/question/{id}")
    //get only one user from its id
    public Question one(@PathVariable Long id) {

        return repo.findById(id)
        .orElseThrow(() -> new UserNotFoundException(id));
    }

    @PostMapping("/question")
    //create a new user
    public Question newQuestion(@RequestBody Question newQuestion){
            return repo.save(newQuestion);
    }
}
