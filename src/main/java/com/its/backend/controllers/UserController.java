package com.its.backend.controllers;

import java.util.List;

import com.its.backend.entities.User;
import com.its.backend.repositories.UserRepository;
import com.its.backend.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    //adding a dependecy
    UserRepository repository;

    @Autowired
    UserService service;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/users")
    //get a list of all users
    public List<User> list(){
        return (List<User>) repository.findAll();
    }
    @GetMapping("/users/{username}")
    //get only one user from its id
    public User one(@PathVariable Long id) {

        return repository.findById(id)
        .orElseThrow(() -> new UserNotFoundException(id));
    }
    @PostMapping("/register")
    //create a new user
    public User newUser(@RequestBody User newUser){
        return service.registerNewUserAccount(newUser);
    }    

    @PutMapping("/users/{id}")
    //update an existing user
    public User replaceUser(@RequestBody User newUser, @PathVariable Long id) {

        return repository.findById(id)
        .map(User -> {
            if(User.getPassword() != null)
            {
                User.setPassword(passwordEncoder.encode(newUser.getPassword()));
            }
            if(!User.getEmail().isEmpty())
            {
                User.setEmail(newUser.getEmail());
            }
            return repository.save(User);
        })
        .orElseThrow(() -> new UserNotFoundException(id));
    }

    @DeleteMapping("/users/{id}")
    //delete a user
    public void deleteUser(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
