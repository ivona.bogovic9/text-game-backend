package com.its.backend.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.its.backend.entities.Character;
import com.its.backend.repositories.CharacterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CharacterController {
    @Autowired
    CharacterRepository repository;

    @GetMapping("/characters")
    public List<Character> list() {
        return (List<Character>) repository.findAll();
    }

    @GetMapping("/characters/{id}")
    public Character one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @GetMapping("/characters/story")
    public String getStory() throws IOException {
        String data = new String(Files.readAllBytes(Paths.get("C:/Users/ivona/Desktop/Fontys/SOFTWARE3/ITS/backend/prequels.txt")));
        return data;
    }
    
    @PostMapping("/characters")
    public Character newCharacter(@RequestBody Character newCharacter){
            return repository.save(newCharacter);
    }    

    @PutMapping("/characters/{id}")
    public Character replaceCharacter(@RequestBody Character newCharacter, @PathVariable Long id) {

        return repository.findById(id)
        .map(Character -> {
            if(!Character.getName().isEmpty())
            {
                Character.setName(newCharacter.getName());
            }
            return repository.save(Character);
        })
        .orElseGet(() -> {
            return repository.save(newCharacter);
        });
    }

    @DeleteMapping("/characters/{id}")
    public void deleteCharacter(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
