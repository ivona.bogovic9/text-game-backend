package com.its.backend.controllers;

public class CharacterNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    CharacterNotFoundException(Long id) {
        super("Could not find Character " + id);
      }
}
