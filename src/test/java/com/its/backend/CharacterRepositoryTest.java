package com.its.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.assertj.core.api.Assertions.*;

import java.util.Optional;

import com.its.backend.entities.Character;
import com.its.backend.entities.Gender;
import com.its.backend.entities.Species;
import com.its.backend.entities.User;
import com.its.backend.repositories.CharacterRepository;
import com.its.backend.repositories.UserRepository;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@DataJpaTest
class CharacterRepositoryTest {
    @Autowired
    CharacterRepository repo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private EntityManager entity;
    @Test
    void saveCharacterTest(){
        User user = new User("lala", "123");
        userRepo.save(user);
        Character character = new Character("lala", Gender.Male, Species.Demon, user);
        repo.save(character);                                                  
        Character character1 = repo.findById(character.getId()).get();
        assertEquals(character.toString(), character1.toString());
    }
    @Test
    void findCharacterFromId(){
        User user = new User("haha", "123");
        userRepo.save(user);
        Character character1 = new Character("baba", Gender.Male, Species.Demon, user);
        repo.save(character1);  
        Optional<Character> c = Optional.of(character1);
        Character characterfound = c.get();
        assertEquals(character1.getId(), characterfound.getId());
    }
    @Test
    void findCharacterFromName(){
        User user = new User("mama", "123");
        userRepo.save(user);
        Character character1 = new Character("blabla", Gender.Male, Species.Demon, user);
        repo.save(character1);
        Character character = repo.findByName(character1.getName());
        assertEquals(character1.toString(), character.toString());
    }
    @Test
    void showAllCharacters(){
        User user1 = new User("Ana", "321");
        User user2 = new User("Leo", "leo");
        entity.persist(user1);
        entity.persist(user2);
        Character character1 = new Character("mama", Gender.Male, Species.Demon, user1);
        Character character2 = new Character("tata", Gender.Male, Species.Demon, user2);
        entity.persist(character1);
        entity.persist(character2);
        Iterable<Character> characters = repo.findAll();
        assertThat(characters).hasSize(2).contains(character1, character2);
    }
    @Test
    void deleteCharacter(){
        User user = new User("Kjala", "722");
        entity.persist(user);
        Character character1 = new Character("star", Gender.Female, Species.Demon, user);
        entity.persist(character1);
        repo.delete(character1);
        assertFalse(repo.existsById(character1.getId()));
    }
    @Test
    void updateCharacter(){
        User user = new User("Al", "277");
        userRepo.save(user);
        Character character = new Character("star", Gender.Female, Species.Demon, user);
        repo.save(character);
        String newName = "Stella";
        character.setName(newName);
        repo.save(character);
        assertEquals(newName, character.getName()); 
    }
}
