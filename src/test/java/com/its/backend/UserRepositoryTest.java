package com.its.backend;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Optional;

import javax.persistence.EntityManager;

import com.its.backend.entities.User;
import com.its.backend.repositories.UserRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@DataJpaTest
class UserRepositoryTest {
    
    @Autowired
    private UserRepository repo;

    @Autowired
    private EntityManager entity;
    
    @Test
    void saveUserTest(){
        User user = new User("lala", "123");
        repo.save(user);
        assertEquals(user.toString(), repo.findByUsername(user.getUsername()).get().toString());
    }  
    @Test 
    void showAllUsersTest(){
        User user1 = new User("Ana", "321");
        entity.persist(user1);
        User user2 = new User("Leo", "leo");
        entity.persist(user2);
        Iterable<User> users = repo.findAll();
        assertThat(users).hasSize(2).contains(user1, user2);
    }
    @Test
    void findUserbyId(){
        User user = new User("Al", "277");
        repo.save(user);
        Optional<User> u = Optional.of(user);
        User userfound = u.get();
        assertEquals(user.getId(), userfound.getId()); 
    }
    @Test
    void updateUser(){
        User user = new User("Seba", "bebe");
        repo.save(user);
        String newUsername = "Mai";
        user.setUsername(newUsername);
        repo.save(user);
        assertEquals(newUsername, user.getUsername()); 
    }
    @Test
    void deleteUser(){
        User user = new User("Kjala", "722");
        entity.persist(user);
        repo.delete(user);
        assertFalse(repo.existsById(user.getId()));
    }
}
